import React from "react";
import Header from './components/Header'
import Perevagi from './components/Perevagi'
import Courses from './components/Courses'
import Parthners from './components/Parthners'
import Certificate from './components/Certificate'
import Team from './components/Team'
import Footer from './components/Footer'

export default function App() {
  return (
    <div className="App">
      <Header />
      <Perevagi />
      <Courses />
      <Parthners />
      <Certificate />
      <Team />
      <Footer />
    </div>
  );
}
