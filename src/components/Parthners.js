import React from 'react'

import Cambridge from './css/img/cambridge.png'
import Harvard from './css/img/harvard-university-logo-90057.png'
import Mit from './css/img/mit_crest_logo.png'
import Ucla from './css/img/ucla_logo.png'

import './css/Parthners.css'

export default function Parthners () {
    return (
        <div id="4" className="parthners">
        <h4 className="parthnerstitle">ПАРТНЕРИ - ОСВІТНІ ЗАКЛАДИ</h4>
        <div className="allparthners">
          <a href="/" id="leftside" className="case"><i className="left"></i></a>
          <div className="parth">
            <img src={Cambridge} alt="" />
            <p className="parthner-name">Cambridge</p>
          </div>
          <div className="parth">
            <img src={Harvard} alt="" />
            <p className="parthner-name"><span className='harvard'>Harvard</span></p>
          </div>
          <div className="parth">
            <img src={Mit} alt="" />
            <p className="parthner-name">MIT</p>
          </div>
          <div className="parth">
            <img src={Ucla} alt="" />
            <p className="parthner-name">UCLA</p>
          </div>
          <a href="/" className="case"><i className="right"></i></a>
        </div>
      </div>
    )
}