import React from 'react'
import './css/Perevagi.css'
import Clock from './css/img/Clock.png'
import Lyre from './css/img/Lyre Filled-100.png'
import iPhone from './css/img/iPhone.png'
import Man from './css/img/Man.png'
import Globe from './css/img/Globe.png'

export default function Perevagi() {
    return (
        <div className="perevagi">
        <h2 className="perevagi-heading">ПЕРЕВАГИ</h2>
        <div className="perevagi-main">
          <div className="firsttwoperevaga">
            <div className="perevaga-box">
              <img src={Clock} alt="clock" /> <br />
              <p className="perevagi-span">Будь-коли</p>
              <br />
              <p className="perevagi-text">
                Навчайтесь у зручний для Вас час. Відеолекції,
              </p>
              <br />
              <p className="perevagi-text">Тести та форум доступні цілодобово.</p>
              <br />
              <p>&nbsp;</p>
            </div>
            <div className="perevaga-box">
              <img src={Globe} alt="clock" /> <br />
              <p className="perevagi-span">Будь-де</p>
              <br />
              <p className="perevagi-text">
                Проходьте курси, не виходячи з дому чи в
              </p>
              <br />
              <p className="perevagi-text">
                дорозі.Все, що Вам знадобиться, – це доступ
              </p>
              <br />
              <p className="perevagi-text">до інтернету.</p>
            </div>
          </div>
          <img
            className="perevagi-logo"
            src={Lyre}
            alt=""
            width="86"
            height="94"
          />
          <div className="lasttwoperevaga">
            <div className="perevaga-box">
              <img src={iPhone} alt="clock" /> <br />
              <p className="perevagi-span">Будь-який пристрій</p>
              <br />
              <p className="perevagi-text">Навчайтесь на комп’ютері, планшеті чи</p>
              <br />
              <p className="perevagi-text">телефоні: сайт підлаштується під Ваш</p>
              <br />
              <p className="perevagi-text">пристрій.</p>
            </div>
            <div className="perevaga-box">
              <img src={Man} alt="clock" /> <br />
              <p className="perevagi-span">Будь-хто</p>
              <br />
              <p className="perevagi-text">
                Наші курси абсолютно безкоштовні – захмарні
              </p>
              <br />
              <p className="perevagi-text">
                ціни більше ніколи не стануть на заваді
              </p>
              <br />
              <p className="perevagi-text">найкращій освіті.</p>
            </div>
          </div>
        </div>
      </div>
    )
}