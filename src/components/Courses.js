import React from 'react'

import './css/Courses.css'

import TA from './css/img/TA_logo_300x300.png'
import Img2 from './css/img/ку5ернаера5ер.png'
import Img3 from './css/img/вкрвкурваеоаеноаноан.png'
import Img4 from './css/img/вкрвнпрвкнрва5егроаеро.png'

export default function Courses () {
    return (
        <div id="3" className="courses">
        <h3 className="courseh">ДОСТУПНІ КУРСИ</h3>
        <p className="coursep">Запишіться прямо зараз на безкоштовні курси</p>
        <div className="generalbox-courses">
          <div className="twocorses">
            <div className="other-course">
              <div className="course-text">
                <p className="course-header">Основи програмування</p>
                <p className="course-description">
                  After the first 50 completed projects to make a mistake with
                  the deadlines almost unreal.
                </p>
                <a href="/" className="readmore">Read more...</a>
                <a href="#6"
                  ><button className="button2">Зареєструватись на курс!</button></a
                >
              </div>
              <img
                className="courses-img"
                src={TA}
                alt="courseimg"
              />
            </div>
            <div className="other-course">
              <div className="course-text">
                <p className="course-header">
                  Економіка <br />
                  для всіх
                </p>
                <p className="course-description">
                  After the first 50 completed projects to make a mistake with
                  the deadlines almost unreal.
                </p>
                <a href="/" className="readmore">Read more...</a>
                <a href="#6"
                  ><button class="button2">Зареєструватись на курс!</button></a
                >
              </div>
              <img
                className="courses-img"
                src={Img2}
                alt="courseimg"
              />
            </div>
          </div>
          <div className="twocorses">
            <div className="other-course">
              <div className="course-text">
                <p className="course-header">
                  Розробка та <br />
                  аналіз алгоритмів
                </p>
                <p className="course-description">
                  After the first 50 completed projects to make a mistake with
                  the deadlines almost unreal.
                </p>
                <a href="/" className="readmore">Read more...</a>
                <a href="#6"
                  ><button className="button2">Зареєструватись на курс!</button></a
                >
              </div>
              <img
                className="courses-img"
                src={Img3}
                alt="courseimg"
              />
            </div>
            <div className="other-course">
              <div className="course-text">
                <p className="course-header">
                  Історія <br />
                  України
                </p>
                <p className="course-description">
                  After the first 50 completed projects to make a mistake with
                  the deadlines almost unreal.
                </p>
                <a href="/" className="readmore">Read more...</a>
                <a href="#6"
                  ><button className="button2">Зареєструватись на курс!</button></a
                >
              </div>
              <img
                className="courses-img"
                src={Img4}
                alt="courseimg"
              />
            </div>
          </div>
        </div>
      </div>
    )
    
}