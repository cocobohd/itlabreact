import React from 'react'

import './css/Certificate.css'

export default function Certificate() {
    return (
        <div className="certificate">
        <div className="certificateinfo">
          <p className="certificatetitle">СЕРТИФІКАТ ВІД ГАРВАРДУ</p>
          <p className="certificatetext">
            Lorem Ipsum is simply dummy text of the printing and <br />
            typesetting industry. Lorem Ipsum has been the <br />
            industry's standard dummy text ever since the 1500s, <br />
            when an unknown printer took a galley of type.
          </p>
          <a href="#6"><button className="button3">ОТРИМАТИ!</button></a>
        </div>
      </div>
    )
}