import React from "react";

import Twitter from './css/img/twitter.png'
import Facebook from './css/img/facebook.png'
import Google from './css/img/google-plus.png'
import Twitterred from './css/img/twitter копия.png'
import Facebookred from './css/img/facebook копия.png'
import Googlered from './css/img/google-plus копия.png'

import './css/Footer.css'

export default function Footer() {
    return(
        <footer id="6" className="footer">
      <h6 className="footertitle">ЗАРЕЄСТРУЙСЯ ЗАРАЗ</h6>
      <p className="footertext">Та отримай задоволення від навчання</p>
      <form action="">
        <input
          className="footer-name"
          type="text"
          name="name"
          placeholder="Name:"
        /><br />
        <input
          className="footer-email"
          type="email"
          name="email"
          placeholder="Email:"
        /><br />
        <button className="button4">Submit</button>
      </form>
      <p className="footerinf">
        Made with love by
        <span className="myname"> Bohdan Hora</span>
      </p>
      <p className="footerinf">
        Ispired by
        <span className="firmname"> PROMETHEUS</span>
      </p>
      <div className="footer-social">
        <a className="soical" href="https://www.twitter.com"
          ><img src={Twitter} alt="twitter"
        /></a>
        <a className="soical" href="https://www.facebook.com/"
          ><img src={Facebook} alt="facebook"
        /></a>
        <a className="soical" href="https://ru.wikipedia.org/wiki/Google%2B"
          ><img src={Google} alt="google+"
        /></a>
      </div>
      <div className="footer-social2">
        <a className="soical" href="https://www.twiiter.com"
          ><img src={Twitterred} alt="twitter"
        /></a>
        <a className="soical" href="https://www.facebook.com/"
          ><img src={Facebookred} alt="facebook"
        /></a>
        <a className="soical" href="https://ru.wikipedia.org/wiki/Google%2B"
          ><img src={Googlered} alt="google+"
        /></a>
      </div>
    </footer>
    )
}