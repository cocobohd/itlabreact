import React from 'react'
import './css/Header.css'

export default function Header () {
    return (
        <header id="1" className="header">
        <nav className="nav">
        <a href="#1" className="logo">COURSEWAY</a>
        <a href="#2" className="navigation">головна</a>
        <a href="#3" className="navigation">курси</a>
        <a href="#4" className="navigation">блог</a>
        <a href="#5" className="navigation">про проект</a>
        <a href="#6" className="navigation">реєстрація</a>
      </nav>
      <h1 id="2" className="firsth">БЕЗКОШТОВНІ ОНЛАЙН-КУРСИ <br /></h1>
      <span className="sub_text">ВІД ВИКЛАДАЧІВ ПРОВІДНИХ УНІВЕРСИТЕТІВ СВІТУ</span>
      <form action="submit" class="form">
        <input className="email" type="email" placeholder="Email:" /><br />
        <input className="password" type="password" placeholder="Password:" /><br />
        <button className="button" type="submit">РОЗПОЧАТИ!</button>
      </form>
    </header>
    )
}