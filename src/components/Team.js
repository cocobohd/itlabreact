import React from "react";

import Team1 from './css/img/team-2.png'
import Team2 from './css/img/team-14.png'
import Team3 from './css/img/team-15.png'
import Team4 from './css/img/team-18.png'
import Facebook from './css/img/social - facebook - 1.png'
import Twitter from './css/img/social - twitter - 1.png'
import Google from './css/img/social - google - plus.png'

import './css/Team.css'

export default function Team () {
    return(
        <div id="5" className="developers">
        <h5 className="developerstitle">РОЗРОБНИКИ</h5>
        <div className="team">
          <div className="personalcard">
            <img src={Team1} alt="team1" />
            <p className="team-name">Daniel Raynolds</p>
            <p className="team-info">
              After the first 50 completed <br />
              projects to make a mistake <br />
              with the deadlines almost <br />
              unreal. After the first hundred <br />
              projects to break the promises <br />
              already impossible.
            </p>
            <div>
              <a className="social" href="https://uk-ua.facebook.com/"
                ><img src={Facebook} alt="facebook"
              /></a>
              <a className="social" href="https://twitter.com/home"
                ><img src={Twitter} alt="twitter"
              /></a>
              <a className="social" href="https://ru.wikipedia.org/wiki/Google%2B"
                ><img src={Google} alt="google+"
              /></a>
            </div>
          </div>
          <div className="personalcard">
            <img src={Team2} alt="team2" />
            <p className="team-name">Nick Parson</p>
            <p className="team-info">
              After the first 50 completed <br />
              projects to make a mistake <br />
              with the deadlines almost <br />
              unreal. After the first hundred <br />
              projects to break the promises <br />
              already impossible.
            </p>
            <div>
              <a className="social" href="https://uk-ua.facebook.com/"
                ><img src={Facebook} alt="facebook"
              /></a>
              <a className="social" href="https://twitter.com/home"
                ><img src={Twitter} alt="twitter"
              /></a>
              <a className="social" href="https://ru.wikipedia.org/wiki/Google%2B"
                ><img src={Google} alt="google+"
              /></a>
            </div>
          </div>
          <div className="personalcard">
            <img src={Team3} alt="team3" />
            <p className="team-name">William Stanley</p>
            <p className="team-info">
              After the first 50 completed <br />
              projects to make a mistake <br />
              with the deadlines almost <br />
              unreal. After the first hundred <br />
              projects to break the promises <br />
              already impossible.
            </p>
            <div>
              <a className="social" href="https://uk-ua.facebook.com/"
                ><img src={Facebook} alt="facebook"
              /></a>
              <a className="social" href="https://twitter.com/home"
                ><img src={Twitter} alt="twitter"
              /></a>
              <a className="social" href="https://ru.wikipedia.org/wiki/Google%2B"
                ><img src={Google} alt="google+"
              /></a>
            </div>
          </div>
          <div>
            <img src={Team4} alt="team4" />
            <p className="team-name">Sarah Noel</p>
            <p className="team-info">
              After the first 50 completed <br />
              projects to make a mistake <br />
              with the deadlines almost <br />
              unreal. After the first hundred <br />
              projects to break the promises <br />
              already impossible.
            </p>
            <div>
              <a className="social" href="https://uk-ua.facebook.com/"
                ><img src={Facebook} alt="facebook"
              /></a>
              <a className="social" href="https://twitter.com/home"
                ><img src={Twitter} alt="twitter"
              /></a>
              <a className="social" href="https://ru.wikipedia.org/wiki/Google%2B"
                ><img src={Google} alt="google+"
              /></a>
            </div>
          </div>
        </div>
      </div>
    )
}